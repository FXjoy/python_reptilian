import time
import random
from pyquery import PyQuery as pq
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import pymongo
import csv

clien=pymongo.MongoClient(host='改成自己的')
db=clien.Singer_ID
coll=db.Name


browser=webdriver.Chrome()
wait=WebDriverWait(browser,5)

def get_singer(page):
    URL='https://music.163.com/#/discover/artist/cat?id=%s'%page
    #print(URL)
    browser.get(URL)
    #switch_to_frame
    #switch_to.frame
    browser.switch_to.frame('g_iframe')
    html=browser.page_source
    doc=pq(html)
    The_name=doc('#m-artist-box .nm').items()
    singername = []
    singerid = []
    for i in The_name:
        #获取歌手与的ID
        NameID=str(i.attr('href')).split('=')[1]
        #获取歌手的名字
        Name=i.text()
        '''
        *****************************
        *     保存到数据库当中      *
        *     Save to the database  *
        *****************************
        
        
        data = {}
        data['歌手的名字'] = Name
        data['歌手的ID']=NameID
        coll.insert_one(data)
        
        '''

        singername.append(NameID)
        singerid.append(Name)
    return zip(singername,singerid)


def get_data(page):
    data=[]
    for singername,singerid in get_singer(page):
        info = {}
        info['歌手名字'] = singername
        info['歌手ID'] = singerid
        data.append(info)
    return data

def The_Csv(page):
    print('正在保存当中 请稍后。。。。。')

    with open('data.csv','a',newline='',encoding='utf8') as p:
        filename=['歌手名字','歌手ID']
        writer=csv.DictWriter(p,fieldnames=filename)
        writer.writeheader()
        data=get_data(page)
        writer.writerows(data)
    print('保存完成 请在本地中查看')
#ID=[1001,1002,1003,2001,2002,2003,6001,6002,6003,7001,7002,7003,4001,4002,4003]
ID=[1001,1002,1003]
for i in ID:

    #print(i)
    The_Csv(i)
    time.sleep(random.randint(0,6))
browser.close()